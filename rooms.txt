{
  "Hall": {
    "south": "Kitchen", "east": "Dining Room", "up": "Attic", "down": "Basement", "item": "key"
   }, 
   "Kitchen": {
     "north": "Hall", "item": "monster"
   }, 
   "Dining Room": {
     "west": "Hall", "south": "Garden", "item": "potion"
   }, 
   "Garden": {
     "north": "Dining Room"
   },
   "Attic": {
     "down": "Hall"
   },
   "Basement": {
     "up": "Hall", "item": "broom"
   }
}
