#!/usr/bin/env python3

import sqlite3
db = 'testdb'
conn = sqlite3.connect(db)
print(f"Opened {db} database successfully")
conn.execute('''CREATE TABLE COMPANY
 (ID INT PRIMARY KEY     NOT NULL,
 NAME           TEXT    NOT NULL,
 AGE            INT     NOT NULL,
 ADDRESS        CHAR(50),
 SALARY         REAL);''')
print(f"Table {db} created successfully")
conn.close()

