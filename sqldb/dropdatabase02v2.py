#!/usr/bin/env python3

import sqlite3
db = 'test.db'
conn = sqlite3.connect('test.db')

print(f"Opened {db} database successfully")
conn.execute('''DROP TABLE COMPANY;''')
print(f"Table {db} dropped successfully")
conn.close()

