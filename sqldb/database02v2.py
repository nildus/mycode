#!/usr/bin/env python3

import sqlite3
db = 'test.db'
conn = sqlite3.connect('test.db')

print(f"Opened {db} database successfully")
conn.execute('''CREATE TABLE IF NOT EXISTS COMPANY
 (ID INT PRIMARY KEY     NOT NULL,
 NAME           TEXT    NOT NULL,
 AGE            INT     NOT NULL,
 ADDRESS        CHAR(50),
 SALARY         REAL);''')
print(f"Table {db} created successfully")
conn.close()

