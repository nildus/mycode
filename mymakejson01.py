#!/usr/bin/python3

import json

# python object(dictionary) to be dumped
webster = {'emp1': {'name': 'Lisa', 'designation': 'programmer', 'age': '34', 'salary': '54000'},'emp2': {'name': 'Elis', 'designation': 'trainee', 'age': '24', 'salary': '40000'}}

# the json file where the output must be stored
with open('webster.json', 'w') as jsonfile:
    json.dump(webster, jsonfile)
