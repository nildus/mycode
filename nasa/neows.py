#!/usr/bin/python3
import requests
import argparse
#import re

## Define NEOW URL
NEOURL = "https://api.nasa.gov/neo/rest/v1/feed?"

# this function grabs our credentials
# it is easily recycled from our previous script
def returncreds():
    ## first I want to grab my credentials
    with open("/home/student/nasa.creds", "r") as mycreds:
        nasacreds = mycreds.read()
    ## remove any newline characters from the api_key
    nasacreds = "api_key=" + nasacreds.strip("\n")
    return nasacreds

# this is our main function
def main():
    ## first grab credentials
    nasacreds = returncreds()

    ## update the date below, if you like
    startdate = "start_date=2022-11-11"
    print(startdate)
#    if args.startdate 
    startdate = args.startdate
    print(startdate)
    input()


    ## the value below is not being used in this
    ## version of the script
    # enddate = "end_date=END_DATE"

    # make a request with the request library
    neowrequest = requests.get(NEOURL + startdate + "&" + nasacreds)

    # strip off json attachment from our response
    neodata = neowrequest.json()

    ## display NASAs NEOW data
    print(neodata)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pass in a start date in following format: YYYY-MM-DD")
    parser.add_argument('--startdate', metavar='StartDate',\
    type=str, default='2012-12-08', help="Pass the date, default is 2012-12-08")
    args = parser.parse_args()
    main()

