#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests
# Import the required library
from geopy.geocoders import Nominatim

NOWURL = "http://api.open-notify.org/iss-now.json"

def main():
    """your code goes below here"""
#    issLocNow = requests.get(f"{APIURL}?lat=100&lon=200")
    issLocNow = requests.get(f"{NOWURL}").json()


    # stuck? you can always write comments
    # Try describe the steps you would take manually

    print(issLocNow)

    lon = issLocNow["iss_position"]["longitude"]
    lat = issLocNow["iss_position"]["latitude"]
    
    print(f"""
    CURRENT LOCATION OF THE ISS:
    Lon: {lon}
    Lat: {lat}
    """)

    # Initialize Nominatim API
    geolocator = Nominatim(user_agent="MyApp")
    location = geolocator.geocode("Marlborough")
    print(f"My location: {location}")

    """
    address = location['address']
    # Traverse the data
    city = address.get('city', '')
    state = address.get('state', '')
    country = address.get('country', '')
    print(f"city: {city}, state: {state}, country: {country}")

    """

    print("The longitude of my location is: ", location.longitude)
    print("The latitude of my location is: ", location.latitude)

    issLon = round(float(lon))
    issLat = round(float(lat))
    print(f"issLon={issLon} issLat={issLat}")

    myLon = round(float(location.longitude))
    myLat = round(float(location.latitude))
    print(f"myLon={myLon} myLat={myLat}")

    if issLon == myLon or issLat == myLat:
        print("Overhead: lon={issLon} and lat={issLat}")

if __name__ == "__main__":
    main()

