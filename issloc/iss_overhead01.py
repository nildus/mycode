#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests

NOWURL = "http://api.open-notify.org/iss-now.json"

def main():
    """your code goes below here"""
#    issLocNow = requests.get(f"{APIURL}?lat=100&lon=200")
    issLocNow = requests.get(f"{NOWURL}").json()


    # stuck? you can always write comments
    # Try describe the steps you would take manually

    print(issLocNow)

    lon= issLocNow["iss_position"]["longitude"]
    lat= issLocNow["iss_position"]["latitude"]
    
    print(f"""
    CURRENT LOCATION OF THE ISS:
    Lon: {lon}
    Lat: {lat}
    """)

if __name__ == "__main__":
    main()

