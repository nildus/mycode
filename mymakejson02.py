#!/usr/bin/python3

# Load JSON file into Python runtime

import json
from pprint import pprint

filename = 'rooms.txt'

with open(filename, 'r') as jsonfile:
    x = json.load(jsonfile)

# x is now python data
pprint(x)

